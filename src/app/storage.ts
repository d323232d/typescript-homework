import { IData } from "./interfaces";

export const getLocalStorage = (key: string): Array<IData> => {
  return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : [];
};

export const setLocalStorage = (key: string, data: Array<IData>): void => {
  localStorage.setItem(key, JSON.stringify(data));
};

export const store: Array<IData> = getLocalStorage('favorite');