import { IData } from "./interfaces";

export const randomNumber = Math.floor(Math.random() * 20);

export const randomMovie = (item: IData): void => {  
  const {
    title,
    overview,
    backdrop_path: image
  } = item
  const randomMovie = document.getElementById('random-movie');
  const randomMovieName = document.getElementById('random-movie-name');
  const randomMovieDescription = document.getElementById('random-movie-description');
  randomMovie.style.background = image ? `url(https://image.tmdb.org/t/p/original/${image}) center top / cover` : '';
  randomMovieName.textContent = title;
  randomMovieDescription.textContent = overview;

}