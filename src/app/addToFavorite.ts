import { IData } from "./interfaces";
import { createMoviesList } from "./moviesList";

export const addToFavorite = (favoriteList: Array<IData>): void => {  
  const favoriteMovies = document.getElementById('favorite-movies');
  favoriteMovies ? favoriteMovies.textContent = '' : null;
  favoriteMovies?.insertAdjacentHTML('afterbegin', createMoviesList(favoriteList, true) ) ;
}