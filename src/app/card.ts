import { IData } from "./interfaces"

export const createCard = (item: IData, isFavorite: boolean):string => {
  const { 
    id,
    poster_path: imgUrl,
    overview: description,
    release_date: date,
  } = item;

  const html = `
  <div class="${isFavorite ? "col-12 p-2" : "col-lg-3 col-md-4 col-12 p-2"}">
    <div class="card shadow-sm" data-id=${id}>
      <img
        src=${imgUrl 
          ? `https://image.tmdb.org/t/p/original/${imgUrl}` 
          : `https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg`}
      />
      <svg 
        xmlns="http://www.w3.org/2000/svg"
        stroke="red"
        fill="red"
        width="50"
        height="50"
        class="bi bi-heart-fill position-absolute p-2"
        data-id=${id}
        viewBox="0 -2 18 22"
      >
        <path
          fill-rule="evenodd"
          d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
        />
      </svg>
      <div class="card-body">
        <p class="card-text truncate">${description}</p>
        <div
          class="
            d-flex
            justify-content-between
            align-items-center
          "
        >
          <small class="text-muted">${date}</small>
        </div>
      </div>
    </div>
  </div>
  `
  return html;
}