import { randomMovie, randomNumber } from "./randomMovie";
import db from "./services";
import { IData } from "./interfaces";
import { renderMoviesList } from "./moviesList";
import { addToFavorite } from "./addToFavorite";
import { store } from "./storage";
import { createFavoriteList } from "./createFavoriteList";

export const init = async (): Promise<void> => {

  const data: IData[] = await db.getAllMovies().then(response => response.results);
  
  randomMovie(data[randomNumber]);
  addToFavorite(store);

  const filmContainer = document.getElementById('film-container');
  const searchInput = document.getElementById('search');
  const searchBtn = document.getElementById('submit');
  const popularBtn = document.getElementById('popular');
  const upcomingBtn = document.getElementById('upcoming');
  const topRatedBtn = document.getElementById('top_rated');
  const loadMoreBtn = document.getElementById('load-more');
  
  renderMoviesList((filmContainer as HTMLElement), data);  
  
  enum FilterStatus {
    popular,
    upcoming, 
    top_rated
  };
  let page = 1;
  let filter = FilterStatus.popular;

  const loadMoreData = (filter: FilterStatus): void => {
    switch (filter) {
      case FilterStatus.popular : 
        page += 1;
        db.getPopularMovies(page).then(response => renderMoviesList((filmContainer as HTMLElement), response.results, page));
        break;
      case FilterStatus.upcoming : 
        page += 1;
        db.getUpcomingMovies(page).then(response => renderMoviesList((filmContainer as HTMLElement), response.results, page));
        break;
      case FilterStatus.top_rated : 
        page += 1;
        db.getTopRateMovies(page).then(response => renderMoviesList((filmContainer as HTMLElement), response.results, page));
        break;
      default: 
        page = 1;
        filter = FilterStatus.popular;
    }
  }

  searchBtn?.addEventListener('click', () => {
      const searchText: string = searchInput ? (searchInput as HTMLInputElement).value : '';
      if(searchText.trim()) {
        db.getSearchMovies(searchText, page).then(response => renderMoviesList((filmContainer as HTMLElement), response.results));      
      }
      (searchInput as HTMLInputElement).value = '';      
  });
  
  popularBtn?.addEventListener('click', () => {
    filter = FilterStatus[popularBtn.id];
    page = 1
    db.getPopularMovies(page).then(response => renderMoviesList((filmContainer as HTMLElement), response.results));
  });

  upcomingBtn?.addEventListener('click', () => {
    filter = FilterStatus[upcomingBtn.id];
    page = 1
    db.getUpcomingMovies(page).then(response => renderMoviesList((filmContainer as HTMLElement), response.results));
  });

  topRatedBtn?.addEventListener('click', () => {
    filter = FilterStatus[topRatedBtn.id];
    page = 1
    db.getTopRateMovies(page).then(response => renderMoviesList((filmContainer as HTMLElement), response.results));
  });

  document.addEventListener('click', e => {
    const target = (e.target as Element);
    if(target && target.closest('.bi')) {
      const parent = target.closest('.card');
      const id = parent ? Number((parent as HTMLElement).dataset.id) : 0;
      createFavoriteList(id)
    }
  }); 

  

  loadMoreBtn?.addEventListener('click', () => loadMoreData(filter));
}

