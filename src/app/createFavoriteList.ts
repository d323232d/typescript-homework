import { setLocalStorage , store} from "./storage";
import { addToFavorite } from "./addToFavorite";
import db from "./services";

export const createFavoriteList = async (id: number): Promise<void> => {
  const existItem = store.length ? store.some(item => Number(item.id) === id) : false;
  if (existItem) {
    const updateStore = store.filter(item => Number(item.id) !== id);
    setLocalStorage('favorite', updateStore);
    addToFavorite(updateStore);
  } else {
    const favorite = await db.getMovieById(id);
    if(favorite) {
      store.push(favorite);
    setLocalStorage('favorite', store);
    addToFavorite(store);
    }
    
  }
};