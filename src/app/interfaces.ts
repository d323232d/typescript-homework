// response
export interface IData {
  [index: string]: string | number;
}

// response.results
export interface IMovieArray {
  [index: number]: IData;
}