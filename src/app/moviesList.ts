import { IData } from "./interfaces"
import { createCard } from "./card";

export const createMoviesList = (response: IData[], isFavorite = false): string => {  
  if (response && response.length) {   
    let list = '';    
    response.forEach( (item): void => {
      list += createCard(item, isFavorite);
    });
    return list;    
  } else {
    return 'No search result';
  }  
};

export const renderMoviesList = ( container: HTMLElement, data: IData[], page = 1): void => {  
  if(container && page === 1) {    
    container.textContent = '';
    container.insertAdjacentHTML('beforeend', createMoviesList(data) ) ;
  }
  if(container && page > 1) {
    container.insertAdjacentHTML('beforeend', createMoviesList(data) ) ;
  }
}