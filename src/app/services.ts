import { IData } from "./interfaces";

const API_KEY = '8851efb5de0b3d680161a820638d2f47';
const API_URL = 'https://api.themoviedb.org/3';

class ServiceDB {
  
  getData = async (url: string) : Promise<IData > => {
    const res = await fetch(url);    
    if(res.ok) {
      return res.json();
    } else {
      throw new Error(`Failed to get data at address ${url}`);
    }
  }

  getAllMovies = (page = 1): Promise<IData> => {
    return this.getData(`${API_URL}/discover/movie?api_key=${API_KEY}&language=en-US&page=${page}&sort_by=popularity.desc&include_adult=false&include_video=false`)
  }

  getSearchMovies = (query: string, page = 1): Promise<IData> => {
    return this.getData(`${API_URL}/search/movie?api_key=${API_KEY}&query=${query}&language=en-US&page=${page}&include_adult=false`)
  }

  getPopularMovies = (page = 1): Promise<IData> => {
    return this.getData(`${API_URL}/movie/popular?api_key=${API_KEY}&language=en-US&page=&page=${page}&include_adult=false&sort_by=popularity.desc`)
  }

  getTopRateMovies = (page = 1): Promise<IData> => {
    return this.getData(`${API_URL}/movie/top_rated?api_key=${API_KEY}&language=en-US&page=&page=${page}&include_adult=false`)
  }

  getUpcomingMovies = (page = 1): Promise<IData> => {
    return this.getData(`${API_URL}/movie/upcoming?api_key=${API_KEY}&language=en-US&page=&page=${page}&include_adult=false`)
  }

  getMovieById = (id: number): Promise<IData> => {
    return this.getData(`${API_URL}/movie/${id}?api_key=${API_KEY}&language=en-US`)
  }


}

const db = new ServiceDB();

export default db;